resource "template_file" "host" {
    template = "${file("host.tpl")}"
    vars = {
      role = "polr"
      policy_url = "https://ScarfaceIII@bitbucket.org/ScarfaceIII/wafflestest.git"
      dns_public_name = "${aws_instance.vm.public_dns}"
    }
}

resource "aws_instance" "vm" {
    connection {
        user = "${var.key_username}"
        key_file = "${var.key_path}"
        agent = true
    }
    ami = "${var.ami}"
    instance_type = "${var.instance_type}"
    key_name = "${var.key_name}"

    associate_public_ip_address = true

    vpc_security_group_ids = ["${aws_security_group.polr-security-group.id}"]

    tags = {
        Name = "XXX"
    }
   

    provisioner "remote-exec" {
      inline = [
        "cd /tmp",
        "sudo apt-get clean && sudo apt-get update",
        "wget -O provision.sh ${var.provision_url}",
        "sudo chmod a+x provision.sh",
        "sudo chown root provision.sh",
        "sudo sed -i -e 's/.*ssh-rsa/ssh-rsa/' /root/.ssh/authorized_keys"
      ]
    }
}

resource "null_resource" "provision" {
    depends_on = ["aws_instance.vm"]
    triggers = {
      instance_id = "${aws_instance.vm.id}"
    }
    connection {
        user = "root"
        key_file = "${var.key_path}"
        agent = true
        host = "${aws_instance.vm.public_ip}"
    }
    provisioner "remote-exec" {
      inline = [
        "cat <<FILEXXX > /dev/shm/host.sh",
        "${template_file.host.rendered}",
        "FILEXXX"
      ]
    }
    provisioner "remote-exec" {
      inline = [
        "/tmp/provision.sh"
      ]
    }
}

resource "null_resource" "serverspec" {                                                               
    depends_on = ["null_resource.provision"]                                                          
    triggers = {                                                                                      
        instance_id = "${aws_instance.vm.id}"                                                         
    }
    connection {
        host = "${aws_instance.vm.public_ip}"                                                         
        user = "root"                                                                                 
        key_file = "${var.key_path}"                                                                  
        agent = true
    }
    provisioner "remote-exec" {                                                                       
        inline = [                                                                                    
           "apt-get install -y ruby2.0",                                                                 
           "gem2.0 install serverspec",                                                               
        ]
    }
    #provisioner "remote-exec" {                                                                      
    #    inline = [                                                                                   
    #       "mysql polr -e "INSERT INTO links VALUES (1,'evviva','http://www.bitcoinaverage.com','5.148.59.10','','0','',0,1,0,'2016-04-27 17:36:44','2016-04-27 17:36:44');"",                                                                
    #    ]
    #}
    provisioner "remote-exec" {                                                                       
        inline = [                                                                                    
           "cd && rm -rf serverspec && mkdir serverspec",
           "cd serverspec && git clone https://ScarfaceIII@bitbucket.org/ScarfaceIII/serverspec.git .",
        ]
    }
}

# set virtualhost document root to polr/public
# chown www-data:ww-data the whole polr download
# Find out why /setup (bootstrap app route) not working ... seems like bootstrap is not  being downloaded
