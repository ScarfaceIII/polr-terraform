#!/bin/bash
set -e

POLICY_URL=""
ROLE=""

if [ -f /dev/shm/host.sh ]; then
        source /dev/shm/host.sh
else
        exit 1
fi

apt-get -y update 
apt-get -y install wget git

# Params
WAFFLES_VERSION="0.21.0"
WAFFLES_TARBALL=https://github.com/jtopjian/waffles/archive/${WAFFLES_VERSION}.tar.gz


if [ POLICY_URL == "" ] || [ ROLE == "" ]; then
        exit 1
fi

cd /root

# fetch and unzip waffles
wget ${WAFFLES_TARBALL} && tar -xvf ${WAFFLES_VERSION}.tar.gz && rm ${WAFFLES_VERSION}.tar.gz
mv waffles-${WAFFLES_VERSION} .waffles

# fetch waffles code
git clone ${POLICY_URL} waffles
echo "data_polr_public_dns=${DNS_PUBLIC_NAME}" >> ~/waffles/data/polr.sh

cd .waffles/site
ln -sf /root/waffles/{profiles,data,roles} .
cd /root/.waffles

./waffles.sh -r ${ROLE}


TOKEN=$(curl -c /tmp/cookie http://${DNS_PUBLIC_NAME}/setup 2>/dev/null  | grep -i token | sed -e "s/.*value='//" -e "s/'.*//")

curl -L -b /tmp/cookie http://${DNS_PUBLIC_NAME}/setup -d "db%3Ahost=localhost&db%3Aport=3306&db%3Ausername=polr&db%3Apassword=password&db%3Aname=polr&app%3Aname=Polr&app%3Aprotocol=http%3A%2F%2F&app%3Aexternal_url=${DNS_PUBLIC_NAME}&setting%3Ashorten_permission=false&setting%3Apublic_interface=true&setting%3Aindex_redirect=&setting%3Abase=32&acct%3Ausername=polr&acct%3Aemail=polr%40admin.tld&acct%3Apassword=polr&app%3Asmtp_server=&app%3Asmtp_port=&app%3Asmtp_username=&app%3Asmtp_password=&app%3Asmtp_from=&app%3Asmtp_from_name=&setting%3Aanon_api=on&setting%3Aauto_api_key=on&setting%3Aregistration_permission=none&setting%3Apassword_recovery=false&app%3Astylesheet=&_token=${TOKEN}" >/dev/null 2>&1
