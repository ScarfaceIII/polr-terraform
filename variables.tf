variable "provision_url" {
    description = "Provisioning script URL"
    default = "http://paste.debian.net/plain/439752"
}

variable "access_key" {
    description = "AWS access key."
}

variable "secret_key" {
    description = "AWS secret key."
}

variable "region" {
    description = "The AWS region to create things in."
    default = "eu-central-1"
}

variable "key_name" {
    description = "Name of the keypair to use in EC2."
    default = "terraform"
}

variable "key_path" {
    descriptoin = "Path to your private key."
    default = "~/.ssh/id_rsa"
}

variable "key_username" {
    descriptoin = "SSH Username"
    default = "vyos"
}

variable "ami" {
    description = "AMI"
    default =  "ami-87564feb"
}

variable "instance_type" {
    description = "Instance Type"
    default = "t2.micro"
}

