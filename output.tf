output "dns-name" {
  value = "${aws_instance.vm.public_dns}"
}
